1. Open `Teraurge.swf` with your favorite flash decompiler.
1. Export `Teraurge_fla/Main_Timeline.as3` to a file.
1. Make a new file in here named with the exact same version it uses for `game_version` in saves. (2.3, 1.11, etc)
1. Use 1.11.yml as a template.
  * MAKE SURE save_variable_names IS THE SAME AS save_variable_names IN THE SWF OR HASHES WILL FAIL.
