'''
Copyright (c) 2018-2019 Teraurged Contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

import hashlib
from teraurged.game_version import GameVersion

def object_checksum(savedata: dict, gameversion: GameVersion) -> str:
    if gameversion.hash_algo == 1:
        return _calc_checksum_v1(savedata, gameversion)

def _calc_checksum_v1(savedata: dict, gameversion: GameVersion) -> str:
    hashgen = ''
    for k in gameversion.save_variable_names:
        addition = ''
        val = savedata[k]
        # Actual code in as3:
        #  _loc2_ = _loc2_ + String(val.toString());
        # Obviously, we have to make some adaptations in Python...

        # Objects.
        if isinstance(val, dict):
            # Object.toString() in AS3 always results in the following, lol
            addition = '[object Object]'
        # Arrays.
        elif isinstance(val, list):
            addition = ','.join(val)
        # Booleans are lowercase in AS3.
        elif isinstance(val, bool):
            addition = str(val).lower()
        # TODO: Haven't run into these yet, thank god.  Might cause problems?
        elif isinstance(val, float):
            addition = str(val)
        else:
            # Everything else is verbatim.
            addition = str(val)

        hashgen += addition
        #if not isinstance(val, (dict, list, bool)):
        #    print(k, addition)
    #print(hashgen)
    return hashlib.md5(hashgen.encode('utf-8')).hexdigest()
