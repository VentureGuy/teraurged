'''
Copyright (c) 2018-2019 Teraurged Contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
import os, yaml, enum, sys
REPO_ROOT = os.path.join(os.path.dirname(os.path.dirname(__file__)))
DATA_DIR  = os.path.join(REPO_ROOT, 'data')

class EVersionFlags(enum.IntFlag):
    NONE = 0
    # Hash algorithm is broken for whatever reason. Implies DEBUG_ON_BY_DEFAULT.
    BUG_BROKEN_HASH_ALGO = 1
    # debug_mode is true by default in this version of the game.
    DEBUG_ON_BY_DEFAULT = 2

    # Mask of known bugs
    ALL_KNOWN_BUGS = 1

class GameVersion(object):
    def __init__(self, version: str):
        self.version = version
        self.save_variable_names = []
        self.hash_algo = 0
        self.stats = []
        self.flags = EVersionFlags.NONE

        self.game_version_file = os.path.join(DATA_DIR, 'game_versions', version+'.yml')
        if not os.path.isfile(self.game_version_file):
            print('!!! Game version {!r} is unsupported. File a bug report with a copy of the game itself attached or linked.'.format(version))
            sys.exit(1)
        with open(os.path.join(DATA_DIR, 'game_versions', version+'.yml'), 'r') as f:
            verdata = yaml.load(f)

            self.save_variable_names = verdata['save_variable_names']
            self.hash_algo           = int(verdata['hash_algo'])
            self.stats               = verdata['stats']

            self.flags               = EVersionFlags.NONE
            for flag in verdata.get('flags',[]):
                if flag not in EVersionFlags.__members__.keys():
                    print('{}: Unknown flag {!r}, skipping'.format(self.game_version_file, flag))
                    continue
                self.flags |= EVersionFlags.__members__[flag].value

    def has(self, flag: EVersionFlags) -> bool:
        return (self.flags & flag) == flag
