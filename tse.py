'''
Copyright (c) 2018-2019 Teraurged Contributors

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in the
Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''
import miniamf.sol
import miniamf
import os
import sys
import platform
import json
import yaml
import argparse

from teraurged.game_version import GameVersion, EVersionFlags
from teraurged.algorithms import object_checksum
from teraurged.amfutils import simplifyAMF

APPDATA = ''
if platform.system() == 'Windows':
    APPDATA = os.getenv('APPDATA')
SOLPATH = os.path.join(APPDATA, 'Teraurge', 'Local Store',
                       '#SharedObjects', 'Teraurge.swf', 'Teraurge.sol')

HASH_KEY = 'savegame_validation'


def _what(args):
    print('Do you need --help?')
    sys.exit(1)
    return


def _show_save(args):
    root = miniamf.sol.load(SOLPATH)
    savegame = root['saved_games'][args.save_id]
    savehash = savegame[HASH_KEY]
    gamevernum = savegame['game_version']
    print(f'{args.save_id}:')
    print(f'  Game Version: {gamevernum}')
    gameversion = GameVersion(gamevernum)
    actualhash = object_checksum(savegame, gameversion)
    print(f'  Verification Checksum: {savehash}')
    if actualhash != savehash:
        print(f'  Calculated Checksum:   {actualhash}')
        if gameversion.has(EVersionFlags.BUG_BROKEN_HASH_ALGO):
            print(f"    * INCORRECT HASH. This is normal in this version, since it has a broken verification algorithm.")
        else:
            print(f"    !!! INCORRECT HASH! Did you mod the save and then forget to run `python tse.py fix <save>`?")
        if gameversion.has(EVersionFlags.DEBUG_ON_BY_DEFAULT):
            print(f"    * Checksums are not checked in this version of the game, since debug_mode is on by default.")
    else:
        print('    * CHECKSUM CORRECT!')
    print('  Player Name: {}'.format(savegame['player_name']))
    print('  Player Stats:')
    for statID, statName in gameversion.stats.items():
        print('    {}: {}'.format(statName, savegame['player_' + statID]))


def _fix_save(args):
    root = miniamf.sol.load(SOLPATH)
    savegame = root['saved_games'][args.save_id]
    gamevernum = savegame['game_version']

    print(f'{args.save_id}:')
    print(f'  Game Version: {gamevernum}')
    gameversion = GameVersion(gamevernum)

    fixes = 0

    savehash = savegame[HASH_KEY]
    actualhash = object_checksum(savegame, gameversion)
    print(f'  Original Checksum:   {savehash}')
    print(f'  Calculated Checksum: {actualhash}')
    if actualhash != savehash:
        if gameversion.has(EVersionFlags.BUG_BROKEN_HASH_ALGO):
            print(f"    * INCORRECT HASH. This is normal in this version, since it has a broken verification algorithm.")
        else:
            print(f"    !!! INCORRECT HASH! Changed to the correct one.")
            fixes += 1
            savegame[HASH_KEY] = savehash = actualhash
        if gameversion.has(EVersionFlags.DEBUG_ON_BY_DEFAULT):
            print(f"    * Checksums are not checked in this version of the game, since debug_mode is on by default.")
    else:
        print('    * CHECKSUM CORRECT!')
    if fixes == 0:
        print('  No fixes were needed.')
    else:
        savegame[HASH_KEY] = savehash = actualhash
        print(f'  {fixes} fixes were applied.')
        root['saved_games'][args.save_id] = savegame
        miniamf.sol.save(root, SOLPATH, encoding=miniamf.AMF3)


def _export_save(args):
    root = miniamf.sol.load(SOLPATH)
    savegame = root['saved_games'][args.save_id]
    if args.filetype == 'auto':
        lfn = args.file.name.lower()
        if lfn.endswith('.yml'):
            args.filetype = 'yaml'
        elif lfn.endswith('.json'):
            args.filetype = 'json'
    savegame = simplifyAMF(savegame)
    if args.filetype == 'yaml':
        args.file.write(
            '# WARNING: Editing this file can break your save if you don\'t know what you\'re doing.\n# Make backups before proceeding.\n')
        yaml.dump(savegame, args.file, default_flow_style=False)
    elif args.filetype == 'json':
        args.file.write('# WARNING: Editing this file can break your save if you don\'t know what you\'re doing.\n# Make backups before proceeding.\n# Oh and JSON doesn\'t actually support comments, so **you need to remove these lines**.\n')
        json.dump(savegame, args.file)
    else:
        print('!!! Unable to detect file format.  Did you forget a file extension (.json, .yml) or --type=(yaml|json)?')
        sys.exit(1)


def _import_save(args):

    if args.filetype == 'auto':
        lfn = args.file.name.lower()
        if lfn.endswith('.yml'):
            args.filetype = 'yaml'
        elif lfn.endswith('.json'):
            args.filetype = 'json'
    savegame = {}
    if args.filetype == 'yaml':
        # Technically, this can load JSON too, but I don't want to take any chances.
        savegame = yaml.load(args.file)
    elif args.filetype == 'json':
        savegame = json.load(args.file)
    else:
        print('!!! Unable to detect file format.  Did you forget a file extension (.json, .yml) or --type=(yaml|json)?')
        sys.exit(1)

    print('Recalculating verification hash...')
    gameversion = GameVersion(savegame['game_version'])
    savegame[HASH_KEY] = object_checksum(savegame, gameversion)
    print('  = ' + savegame[HASH_KEY])

    root = miniamf.sol.load(SOLPATH)
    root['saved_games'][args.save_id] = savegame
    miniamf.sol.save(root, SOLPATH, encoding=miniamf.AMF3)
    print(f'{args.save_id} saved to Teraurge.sol.')


def main():
    argp = argparse.ArgumentParser(
        description='Teraurge Save Editor - Utility for modifying or examining saves from the game Teraurge.')
    argp.set_defaults(func=_what)
    subp = argp.add_subparsers()

    # tse show j1
    showp = subp.add_parser('show', help="Displays the specified save.")
    showp.add_argument('save_id', help="Save ID")
    showp.set_defaults(func=_show_save)

    # tse fix j1
    fixp = subp.add_parser(
        'fix', help="Fixes common save screwups.  At the moment, only corrects save checksums, like what might happen if you edited your SOL with minerva.")
    fixp.add_argument('save_id', help="Save ID")
    fixp.set_defaults(func=_fix_save)

    # tse export j1 j1.yml [--type=json|yaml|auto]
    exportp = subp.add_parser('export', help="Exports save to JSON or YAML.")
    exportp.add_argument('save_id', help="Save ID")
    exportp.add_argument('file', type=argparse.FileType('w', encoding='utf-8'),
                         help="File to save to.  Extension will be used to determine what format to use.")
    exportp.add_argument('--type', dest="filetype", choices=[
                         'json', 'yaml', 'auto'], default='auto', help="What kind of serialization format to use.")
    exportp.set_defaults(func=_export_save)

    # tse import j1.yml j1 [--type=json|yaml|auto]
    importp = subp.add_parser('import', help="Imports save from JSON or YAML.")
    importp.add_argument('file', type=argparse.FileType('r', encoding='utf-8-sig'),
                         help="File to load from.  Extension will be used to determine what format to use.")
    importp.add_argument('save_id', help="Destination save ID")
    importp.add_argument('--type', dest="filetype", choices=[
                         'json', 'yaml', 'auto'], default='auto', help="What kind of serialization format to use.")
    importp.set_defaults(func=_import_save)

    args = argp.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
