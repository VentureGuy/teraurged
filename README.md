# TeraurgEd
<blockquote>*A <abbr title="Proof of Concept">PoC</abbr> Teraurge save editor.*</blockquote>

This software allows users of Teraurge to export, modify, import, and (potentially) fix saves stored in the Teraurge.sol file.

**WARNING:** This software is not affiliated with, created by, supported by, nor encouraged by the Teraurge developer. By using it, you acknowledge that misuse could corrupt your saves. **USE AT YOUR OWN RISK.**  The built-in debug menu is a far better and safer method for cheating.


# WHY?

I wanted to be able to examine and modify saves used in Teraurge, as one of my hobbies is reverse-engineering things (the VentureGuy alias has released some of my adult game mods, but I have other projects, as well).  

However, at the time of writing, Teraurge 2.3 (and 2.4) use a checksum to verify that saves have not been modified.  When the user loads a save, the following occurs:

1. The game deserializes the save from the SharedObject.
1. The game then iterates over every entry in the returned object, and calls `.toString()` on it, adding the result to a string.
1. The string is hashed with `com.adobe.crypto.MD5.hash()` from a popular AS3 library embedded in the AIR SWF.
1. The hash is compared with the hash generated when the save was made.
1. If the hashes match, the save is loaded into memory.

So, to mod a save, you'd have to recalculate the hash embedded in the save.  Unfortunately, this is not a simple task, since you have to emulate `.toString()` and iteration behavior in your preferred programming language.

So I made this, with plans for toggling preferences and a full Qt-based GUI for people allergic to CLI.

# Requirements

* `>= Python 3.6`
* `pip install pyyaml git+https://github.com/zackw/mini-amf.git `

# Usage

## General
```
usage: tse.py [-h] {show,fix,export,import} ...

Teraurge Save Editor - Utility for modifying or examining saves from the game
Teraurge.

positional arguments:
  {show,fix,export,import}
    show                Displays the specified save.
    fix                 Fixes common save screwups. At the moment, only
                        corrects save checksums, like what might happen if you
                        edited your SOL with minerva.
    export              Exports save to JSON or YAML.
    import              Imports save from JSON or YAML.

optional arguments:
  -h, --help            show this help message and exit
```

## Show
```
usage: tse.py show [-h] save_id

positional arguments:
  save_id     Save ID

optional arguments:
  -h, --help  show this help message and exit
```

## Fix

This currently only fixes incorrect hashes.

```
usage: tse.py fix [-h] save_id

positional arguments:
  save_id     Save ID

optional arguments:
  -h, --help  show this help message and exit
```

## Export
This lets you export a given save to [YAML](https://yaml.org) or JSON.  I may add TOML or CSON later.

```
usage: tse.py export [-h] [--type {json,yaml,auto}] save_id file

positional arguments:
  save_id               Save ID
  file                  File to save to. Extension will be used to determine
                        what format to use.

optional arguments:
  -h, --help            show this help message and exit
  --type {json,yaml,auto}
                        What kind of serialization format to use.
```

The output is a raw representation of the save.  **IF YOU EDIT, PRESERVE TYPES.**

## Import
This lets you import a save from a YAML or JSON file.

```
usage: tse.py import [-h] [--type {json,yaml,auto}] file save_id

positional arguments:
  file                  File to load from. Extension will be used to determine
                        what format to use.
  save_id               Destination save ID

optional arguments:
  -h, --help            show this help message and exit
  --type {json,yaml,auto}
                        What kind of serialization format to use.
```

TSE will also recalculate the hash for you.  **NO VALIDATION IS PERFORMED ON THE SAVE AFTER LOADING, SO IF YOU DID SOMETHING WRONG, TERAURGE WILL NOT BE HAPPY.**

# License

Teraurged is available under the MIT Open Source License.
